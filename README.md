Documentation

Installer un projet Angular existant :
*Clonez le dépôt depuis GitLab avec le lien suivant : https://gitlab.com/clairekondrakhin.dev/spend-all-angular
*Exécutez npm install pour installer les dépendances.
*Utilisez ng serve pour lancer le serveur de développement.
*Accédez à http://localhost:4200/ dans votre navigateur.

Initialisation d'un projet Angular:

Prérequis :
1/Node.js installé sur votre machine. Vous pouvez le télécharger depuis le site officiel de Node.js.
2/Un éditeur de texte ou un IDE, comme Visual Studio Code, installé sur votre machine.

Etapes :

Ouvrir votre IDE
ouvrir le terminal
Exécutez la commande npm install -g @angular/cli pour installer Angular CLI globalement sur votre machine
Vous pouvez vérifier si Angular CLI a été installé correctement en tapant ng --version
Utilisez la commande suivante pour créer un nouveau projet Angular: ng new NomDuProjet
Vous serez invité à choisir différentes options, comme le style de feuille de style (CSS, SCSS, etc.) et si vous souhaitez ajouter Angular Routing. Vous pouvez appuyer sur "Enter" pour utiliser les options par défaut.
Utilisez la commande suivante pour lancer le serveur de développement : ng serve
Cette commande démarrera le serveur de développement, et vous pourrez accéder à votre application Angular dans votre navigateur à l'adresse http://localhost:4200/
Vous pouvez arrêter le serveur de développement en appuyant sur Ctrl + C dans le terminal.